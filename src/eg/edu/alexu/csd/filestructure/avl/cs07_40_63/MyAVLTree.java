package eg.edu.alexu.csd.filestructure.avl.cs07_40_63;

import eg.edu.alexu.csd.filestructure.avl.IAVLTree;
import eg.edu.alexu.csd.filestructure.avl.INode;

public class MyAVLTree<T extends Comparable<T>> implements IAVLTree<T> {

	MyNode<T> root;

	// A utility function to get height of the tree
	int height(MyNode<T> node) {
		if (node == null)
			return 0;
		return node.height;
	}

	// A utility function to right rotate subtree rooted with y (grandParent)
	MyNode<T> rightRotate(MyNode<T> x) {
		MyNode<T> y = x.left;
		MyNode<T> z = y.right;

		// Perform rotation
		y.right = x;
		x.left = z;

		// Update heights
		x.height = Math.max(height(x.left), height(x.right)) + 1;
		y.height = Math.max(height(y.left), height(y.right)) + 1;

		// Return new root (parent)
		return y;
	}

	// A utility function to left rotate subtree rooted with x
	MyNode<T> leftRotate(MyNode<T> x) {
		MyNode<T> y = x.right;
		MyNode<T> z = y.left;

		// Perform rotation
		y.left = x;
		x.right = z;

		// Update heights
		x.height = Math.max(height(x.left), height(x.right)) + 1;
		y.height = Math.max(height(y.left), height(y.right)) + 1;

		// Return new root
		return y;
	}

	// Get Balance factor of node N
	int getBalance(MyNode<T> node) {
		if (node == null)
			return 0;
		return height(node.left) - height(node.right);
	}

	MyNode<T> insert(MyNode<T> node, T key) {

		/* 1. Perform the normal BST rotation */
		if (node == null)
			return (new MyNode<T>(key));

		if (((Comparable<T>) key).compareTo(node.key) < 0)
			node.left = insert(node.left, key);
		else if (((Comparable<T>) key).compareTo(node.key) >= 0)
			node.right = insert(node.right, key);
		// else // Equal keys not allowed
		// return node;

		/* 2. Update height of this ancestor node */
		node.height = 1 + Math.max(height(node.left), height(node.right));

		/*
		 * 3. Get the balance factor of this ancestor node to check whether this node
		 * became Wunbalanced
		 */
		int balance = getBalance(node);

		// If this node becomes unbalanced, then
		// there are 4 cases Left Left Case
		if (balance > 1 && ((Comparable<T>) key).compareTo(node.left.key) < 0)
			return rightRotate(node);

		// Right Right Case
		if (balance < -1 && ((Comparable<T>) key).compareTo(node.right.key) > 0)
			return leftRotate(node);

		// Left Right Case
		if (balance > 1 && ((Comparable<T>) key).compareTo(node.left.key) > 0) {
			node.left = leftRotate(node.left);
			return rightRotate(node);
		}

		// Right Left Case
		if (balance < -1 && ((Comparable<T>) key).compareTo(node.right.key) < 0) {
			node.right = rightRotate(node.right);
			return leftRotate(node);
		}

		/* return the (unchanged) node pointer */
		return node;
	}

	@Override
	public void insert(T key) {
		root = insert(root, key);
	}

	// Given a non-empty binary search tree, return the node with minimum key
	MyNode<T> minValueNode(MyNode<T> node) {
		MyNode<T> current = node;

		/* loop down to find the leftmost leaf */
		while (current.left != null)
			current = current.left;

		return current;
	}

	MyNode<T> deleteNode(MyNode<T> root, T key) {
		// STEP 1: PERFORM STANDARD BST DELETE
		if (root == null)
			return root;

		// If the key to be deleted is smaller than
		// the root's key, then it lies in left subtree
		if (((Comparable<T>) key).compareTo(root.key) < 0)
			root.left = deleteNode(root.left, key);

		// If the key to be deleted is greater than the
		// root's key, then it lies in right subtree
		else if (((Comparable<T>) key).compareTo(root.key) > 0)
			root.right = deleteNode(root.right, key);

		// if key is same as root's key, then this is the node
		// to be deleted
		else {

			// node with only one child or no child
			if ((root.left == null) || (root.right == null)) {
				MyNode<T> temp = null;
				if (temp == root.left)
					temp = root.right;
				else
					temp = root.left;

				// No child case
				if (temp == null) {
					temp = root;
					root = null;
				} else // One child case
					root = temp; // Copy the contents of
									// the non-empty child
			} else {

				// node with two children: Get the inorder
				// successor (smallest in the right subtree)
				MyNode<T> temp = minValueNode(root.right);

				// Copy the inorder successor's data to this node
				root.key = temp.key;

				// Delete the inorder successor
				root.right = deleteNode(root.right, temp.key);
			}
		}

		// If the tree had only one node then return
		if (root == null)
			return root;

		// STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
		root.height = Math.max(height(root.left), height(root.right)) + 1;

		// STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
		// this node became unbalanced)
		int balance = getBalance(root);

		// If this node becomes unbalanced, then there are 4 cases
		// Left Left Case
		if (balance > 1 && getBalance(root.left) >= 0)
			return rightRotate(root);

		// Left Right Case
		if (balance > 1 && getBalance(root.left) < 0) {
			root.left = leftRotate(root.left);
			return rightRotate(root);
		}

		// Right Right Case
		if (balance < -1 && getBalance(root.right) <= 0)
			return leftRotate(root);

		// Right Left Case
		if (balance < -1 && getBalance(root.right) > 0) {
			root.right = rightRotate(root.right);
			return leftRotate(root);
		}

		return root;
	}

	@Override
	public boolean delete(T key) {
		boolean check = search(key);
		if (check == false) {
			return false;
		} else {
			root = deleteNode(root, key);
			return true;
		}
	}

	private boolean search(MyNode<T> node, T key) {
		if (node == null)
			return false;
		if (((Comparable<T>) key).compareTo(node.key) > 0)
			return search(node.right, key);
		else if (((Comparable<T>) key).compareTo(node.key) < 0)
			return search(node.left, key);
		else
			return true;
	}

	@Override
	public boolean search(T key) {
		return search(root, key);
	}

	@Override
	public int height() {
		if (root == null)
			return 0;
		return this.root.height;
	}

	@Override
	public INode<T> getTree() {
		return this.root;
	}

	public void preOrder(MyNode<T> node) {
		if (node != null) {
			System.out.print(node.key + " ");
			preOrder(node.left);
			preOrder(node.right);
		}
	}
}

class MyNode<T extends Comparable<T>> implements INode<T> {

	public T key;
	public int height;
	public MyNode<T> left, right;

	public MyNode(T key) {
		this.key = key;
		this.height = 1;
	}

	@Override
	public INode<T> getLeftChild() {
		return this.left;
	}

	@Override
	public INode<T> getRightChild() {
		return this.right;
	}

	@Override
	public T getValue() {
		return this.key;
	}

	@Override
	public void setValue(T value) {
		this.key = value;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
