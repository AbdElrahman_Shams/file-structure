
package eg.edu.alexu.csd.filestructure.avl.cs07_40_63;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import eg.edu.alexu.csd.filestructure.avl.IDictionary;

public class MyDictionary implements IDictionary {

	private MyAVLTree<String> avl;
	private int size;

	public MyDictionary() {

		avl = new MyAVLTree<String>();
		size = 0;
	}

	@Override
	public void load(File file) {

		String[] wordsArray = null;
		BufferedReader br = null;
		String words;
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line + " ");
				line = br.readLine();
			}
			words = sb.toString();
			wordsArray = words.split(" ");
		} catch (FileNotFoundException e) {
			e.printStackTrace(); // File Not Found Error
		} catch (IOException e) {
			e.printStackTrace(); // IO Error
		}
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace(); // closing file error
		}
		for (String word : wordsArray) {
			if (!avl.search(word)) {
				avl.insert(word);
				++size;
			}
		}
	}

	@Override
	public boolean insert(String word) {

		if (!avl.search(word)) {
			avl.insert(word);
			++size;
			return true;
		}
		System.out.println("ERROR: Word already in the dictionary!");
		return false;
	}

	@Override
	public boolean exists(String word) {

		return avl.search(word);
	}

	@Override
	public boolean delete(String word) {

		if (avl.search(word)) {
			--size;
		}
		return avl.delete(word);
	}

	@Override
	public int size() {

		return size;
	}

	@Override
	public int height() {

		return avl.height();
	}

}