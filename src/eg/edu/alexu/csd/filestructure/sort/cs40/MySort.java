package eg.edu.alexu.csd.filestructure.sort.cs40;

import java.util.ArrayList;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.ISort;

public class MySort<T extends Comparable<T>> implements ISort<T> {

	@Override
	public IHeap<T> heapSort(ArrayList<T> unordered) {
		MyHeapSort<T> myHeapSort = new MyHeapSort<>();
		MyHeap<T> heap = (MyHeap<T>) myHeapSort.heapSort(unordered);

		heap.setHeapSize(unordered.size());

		return heap;
	}

	@Override
	public void sortSlow(ArrayList<T> unordered) {

		SortSlow<T> sorter = new SortSlow<>();
		sorter.bubbleSort(unordered);

	}

	@Override
	public void sortFast(ArrayList<T> unordered) {

		SortFast<T> sorter = new SortFast<>();
		sorter.sort(unordered);

	}

}
