package eg.edu.alexu.csd.filestructure.sort.cs40;

import java.util.ArrayList;

//**********BubbleSort**********//
public class SortSlow<T extends Comparable<T>> {
	public final void bubbleSort(final ArrayList<T> values) {

		int numOfElements = values.size();

		if (numOfElements == 0 || values == null) {
			return;
		}
		T temp;
		for (int i = 0; i < numOfElements; i++) {
			for (int j = 1; j < (numOfElements - i); j++) {

				if (values.get(j - 1).compareTo(values.get(j)) > 0) {
					temp = values.get(j - 1);
					values.set(j - 1, values.get(j));
					values.set(j, temp);
				}
			}
		}
	}
}