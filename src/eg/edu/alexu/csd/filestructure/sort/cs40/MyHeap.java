package eg.edu.alexu.csd.filestructure.sort.cs40;

import java.util.ArrayList;
import java.util.Collection;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyHeap<T extends Comparable<T>> implements IHeap<T> {

	private ArrayList<INode<T>> heapArray;
	private int heapSize;

	public MyHeap() {
		heapArray = new ArrayList<INode<T>>();
		heapSize = 0;
	}

	@Override
	public INode<T> getRoot() {
		if (heapSize > 0) {
			return heapArray.get(0);
		}
		return null;
	}

	@Override
	public int size() {
		return heapSize;
	}

	// this function swaps values of two nodes
	private void swap(INode<T> firstNode, INode<T> secondNode) {

		T temp = firstNode.getValue();

		firstNode.setValue(secondNode.getValue());
		secondNode.setValue(temp);

	}

	public void setHeapSize(int heapSize) {
		this.heapSize = heapSize;
	}

	@Override
	public void heapify(INode<T> node) {

		INode<T> leftChild = node.getLeftChild();
		INode<T> rightChild = node.getRightChild();

		INode<T> largest;

		if (leftChild != null && leftChild.getValue().compareTo(node.getValue()) > 0) {
			largest = leftChild;
		} else {
			largest = node;
		}
		if (rightChild != null && rightChild.getValue().compareTo(largest.getValue()) > 0) {
			largest = rightChild;
		}
		if (!largest.equals(node)) {
			swap(node, largest);
			heapify(largest);
		}

	}

	@Override
	public T extract() {
		if (heapSize <= 0) {
			return null;
		}

		T largest = getRoot().getValue();
		swap(getRoot(), heapArray.get(heapSize - 1));
		heapSize--;

		if (heapSize > 0) {
			heapify(getRoot());
		}

		return largest;
	}

	@Override
	public void insert(T element) {

		if (heapSize > 0 && !element.equals(null)) {
			if (!getRoot().equals(null)) {
				if (element.compareTo(getRoot().getValue()) > 1) {

					heapArray.add(0, new MyNode(element, 0));

					for (int i = 1; i < heapArray.size(); i++) {
						((MyNode) heapArray.get(i)).setIndex(i);
					}

					heapSize = heapArray.size();

				} else {

					heapArray.add(new MyNode(element, heapSize));
					heapSize = heapArray.size();

					if (heapSize > 1 && getRoot() != null) {
						heapify(getRoot());
					}
				}
			}
		} else if (heapSize == 0) {

			heapArray.add(new MyNode(element, 0));

			heapSize++;

		} else {
			throw null;
		}
	}

	@Override
	public void build(Collection<T> unordered) {

		heapSize = unordered.size();
		int i = 0;

		for (T t : unordered) {
			heapArray.add(new MyNode(t, i));
			i++;
		}

		for (int j = (size() - 1) / 2; j >= 0; j--) {

			if (size() > 0) {
				heapify(heapArray.get(j));
			}
		}

	}

	class MyNode implements INode<T> {

		private int index;
		private T value;

		MyNode(T value, int index) {

			this.index = index;
			this.value = value;
		}

		@Override
		public INode<T> getLeftChild() {
			int leftChildIndex = 2 * (index + 1) - 1;

			if (leftChildIndex < size() && leftChildIndex > 0) {
				return heapArray.get(leftChildIndex);
			}
			return null;
		}

		@Override
		public INode<T> getRightChild() {

			int rightChildIndex = (2 * (index + 1));

			if (rightChildIndex < size() && rightChildIndex > 0) {
				return heapArray.get(rightChildIndex);
			}
			return null;
		}

		@Override
		public INode<T> getParent() {

			int parentIndex = (index - 1) / 2;

			if (parentIndex < size() && parentIndex > 0) {
				return heapArray.get(parentIndex);
			}
			return null;
		}

		@Override
		public T getValue() {
			return value;
		}

		@Override
		public void setValue(T value) {
			this.value = value;
		}

		public void setIndex(int index) {
			this.index = index;
		}

	}
}
